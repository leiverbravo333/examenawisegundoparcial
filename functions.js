const student = async (request, response) => {
	const expresiones = {
	  palabras: /^([a-zA-Z]+)*$/,
	  cedula: /^\d{10}$/,
	  letras: /^[A-Za-z]{1,9}/,
	  correo: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
	};
  
	const { cedula, apellidos, nombres, direccion, semestre, paralelo, correo } = request.body;
  
	if (
		  !expresiones.cedula.test(cedula) ||
		  !expresiones.palabras.test(apellidos) ||
		  !expresiones.palabras.test(nombres) ||
		  !expresiones.palabras.test(direccion) ||
		  !expresiones.letras.test(semestre) ||
		  !expresiones.letras.test(paralelo) ||
		  !expresiones.correo.test(correo)
		) {
		  response.json({ error: "true" });
		  } else {
			  try {
			  const newEst = await pool.query(
				  "INSERT INTO estudiante(cedula, nombre, apellido, direccion, semestre, paralelo) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING *",
				  [cedula, nombres, apellidos, direccion, semestre, paralelo, correo]
			  );
			  if (newEst.rowCount == 1) {
				  response.json(newEst.rows);
			  }
			  } catch (error) {
			  response.json({ error: error });
			  }
		  }
};

var getData = function () {
        var cedula = document.getElementById("cedula").value;
        var apellidos = document.getElementById("apellidos").value;
        var nombres = document.getElementById("nombres").value;
        var direccion = document.getElementById("direccion").value;
        var semestre = document.getElementById("semestre").value;
        var paralelo = document.getElementById("paralelo").value;
        var correo = document.getElementById("correo").value;
        if (cedula == "") {
          document.getElementById("cedula").focus();
        } else {
          if (apellidos == "") {
            document.getElementById("apellidos").focus();
          } else {
            if (nombres == "") {
              document.getElementById("nombres").focus();
            } else {
              if (direccion == "") {
                document.getElementById("direccion").focus();
              } else {
                if (semestre == "") {
                  document.getElementById("semestre").focus();
                } else {
                  if (paralelo == "") {
                    document.getElementById("paralelo").focus();
                  } else {
                    if (correo == "") {
                      document.getElementById("correo").focus();
                    } else {
                      console.log(cedula + " " + apellidos + " " + nombres + " " + direccion + " " + semestre + " " + paralelo + " " + correo);

                      document.getElementById("cedula").value = "";
                      document.getElementById("apellidos").value = "";
                      document.getElementById("nombres").value = "";
                      document.getElementById("direccion").value = "";
                      document.getElementById("semestre").value = "";
                      document.getElementById("paralelo").value = "";
                      document.getElementById("correo").value = "";
                    }
                  }
                }
              }
            }
          }
        }
      };

